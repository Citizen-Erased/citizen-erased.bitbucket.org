var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Prijava v sistem z privzetim uporabnikom za predmet OIS in pridobitev
 * enolične ID številke za dostop do funkcionalnosti
 * @return enolični identifikator seje za dostop do funkcionalnosti
 */
function getSessionId() {
	var response = $.ajax({
		type: "POST",
		url: baseUrl + "/session?username=" + encodeURIComponent(username) +
			"&password=" + encodeURIComponent(password),
		async: false
	});
	return response.responseJSON.sessionId;
}

//osnovni podatki o osebah, ki jih generiramo
var podatkiZaGeneritat = '{ "pacient" : [' +
	'{ "firstName":"Jaka", "lastName":"Slivar", "visina":"145", "teza":"72", "srcniUtrip":"97"},' +
	'{ "firstName":"Luka", "lastName":"Hostnik", "visina":"192", "teza":"97", "srcniUtrip":"154"},' +
	'{ "firstName":"Oto", "lastName":"Avsenik", "visina":"163", "teza":"69", "srcniUtrip":"92"} ]}';

var obj = JSON.parse(podatkiZaGeneritat);

/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
var stPacientovIzpisanih = 0;

function generirajPodatke() {
	//ce so trije pacienti ze izpisani jih izbrisemo
	if(stPacientovIzpisanih == 3){
		$("#kreiraniEHRji").text("");
		stPacientovIzpisanih = 0;
	}
	
	for(var stPacienta = 0; stPacienta < 3; stPacienta++, stPacientovIzpisanih++){
		generirajPodatke(stPacienta);
	}
	
	function generirajPodatke(i) {
		var ime, priimek, visina, teza, srcniUtrip;
	
		sessionId = getSessionId();
	
		ime = obj.pacient[i].firstName;
		priimek = obj.pacient[i].lastName;
		visina = obj.pacient[i].visina;
		teza = obj.pacient[i].teza;
		srcniUtrip = obj.pacient[i].srcniUtrip;
		
	
		$.ajaxSetup({
			headers: {
				"Ehr-Session": sessionId
			}
		});
		$.ajax({
			url: baseUrl + "/ehr",
			type: 'POST',
			success: function(data) {
				var ehrId = data.ehrId;
				var partyData = {
					firstNames: ime,
					lastNames: priimek,
					partyAdditionalInfo: [{
						key: "ehrId",
						value: ehrId
					}]
				};
				$.ajax({
					url: baseUrl + "/demographics/party",
					type: 'POST',
					contentType: 'application/json',
					data: JSON.stringify(partyData),
					success: function(party) {
						if (party.action == 'CREATE') {
							zapisiSePodatke(ehrId);
						}
					},
	
				});
	
			
			
				function zapisiSePodatke(ehrId) {
					$.ajaxSetup({
						headers: {
							"Ehr-Session": sessionId
						}
					});
					var podatki = {
						"ctx/language": "en",
						"ctx/territory": "SI",
	
						"vital_signs/height_length/any_event/body_height_length": visina,
						"vital_signs/body_weight/any_event/body_weight": teza,
						"vital_signs/pulse/any_event/rate|magnitude": srcniUtrip,
						"vital_signs/pulse/any_event/rate|unit": "/min",
						
					};
					var parametriZahteve = {
						ehrId: ehrId,
						templateId: 'Vital Signs',
						format: 'FLAT',
						// committer: merilec
					};
					$.ajax({
						url: baseUrl + "/composition?" + $.param(parametriZahteve),
						type: 'POST',
						contentType: 'application/json',
						data: JSON.stringify(podatki),
						success: function(res) {
							document.getElementById("kreiraniEHRji").innerHTML += "<p class='bg-success'> <b>Generiran vpis:</b> " + ime + " " + priimek + ",<b> EHR-Id:</b> " + ehrId + "</p>"
						},
					});
				}
			}
		});
	}
}

function kreirajEHRzaBolnika() {
	sessionId = getSessionId();

	var ime = $("#kreirajIme").val();
	var priimek = $("#kreirajPriimek").val();

	if (!ime || !priimek || ime.trim().length == 0 ||
		priimek.trim().length == 0) {
		$("#kreirajSporocilo").html("<br><span class='obvestilo label " +
			"label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
			headers: {
				"Ehr-Session": sessionId
			}
		});
		$.ajax({
			url: baseUrl + "/ehr",
			type: 'POST',
			success: function(data) {
				var ehrId = data.ehrId;
				var partyData = {
					firstNames: ime,
					lastNames: priimek,
					partyAdditionalInfo: [{
						key: "ehrId",
						value: ehrId
					}]
				};
				$.ajax({
					url: baseUrl + "/demographics/party",
					type: 'POST',
					contentType: 'application/json',
					data: JSON.stringify(partyData),
					success: function(party) {
						if (party.action == 'CREATE') {
							$("#kreirajSporocilo").html("<br><span class='obvestilo " +
								"label label-success fade-in'>Uspešno kreiran EHR '" +
								ehrId + "'.</span>");
							$("#preberiEHRid").val(ehrId);
						}
					},
					error: function(err) {
						$("#kreirajSporocilo").html("<br><span class='obvestilo label " +
							"label-danger fade-in'>Napaka '" +
							JSON.parse(err.responseText).userMessage + "'!");
					}
				});
			}
		});
	}
}


$(document).ready(function() {
	$("#wikiDetails").hide();
	$("#friendlyTip").hide();

    $("#wiki").hide();

	$("#overall").hide();
	$("#details").hide();

});

var wikiDisplayed = false;

function prikaziWiki() {
	var ehrId = $("#meritveVitalnihZnakovEHRid").val();
    if(!wikiDisplayed && ehrId.length != 0){
        $("#wiki").toggle();
        wikiDisplayed = true;
    }

}



var stevec = 0;

function preveri() {
	stevec++;

	if (stevec == 5) {
		document.getElementById('bubbles').innerHTML = '';
		zakasnitev();
		stevec = 0;
	}

}

function dodajMeritveVitalnihZnakov() {
	sessionId = getSessionId();

	var ehrId = $("#dodajVitalnoEHR").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
	var srcniUtrip = $("#dodajVitalnoSrcniUtrip").val();
	

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<br><span class='obvestilo " +
			"label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		$.ajaxSetup({
			headers: {
				"Ehr-Session": sessionId
			}
		});
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
			// https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
			"ctx/language": "en",
			"ctx/territory": "SI",
			// "ctx/time": datumInUra,
			"vital_signs/height_length/any_event/body_height_length": telesnaVisina,
			"vital_signs/body_weight/any_event/body_weight": telesnaTeza,
			"vital_signs/pulse/any_event/rate|magnitude": srcniUtrip,
			"vital_signs/pulse/any_event/rate|unit": "/min",
			//"vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
			ehrId: ehrId,
			templateId: 'Vital Signs',
			format: 'FLAT',
			// committer: merilec
		};
		$.ajax({
			url: baseUrl + "/composition?" + $.param(parametriZahteve),
			type: 'POST',
			contentType: 'application/json',
			data: JSON.stringify(podatki),
			success: function(res) {
				$("#dodajMeritveVitalnihZnakovSporocilo").html(
					"<br><span class='obvestilo label label-success fade-in'>Uspešno dodajanje meritve k EHR ID '" + ehrId + "'.</span>");
			},
			error: function(err) {
				$("#dodajMeritveVitalnihZnakovSporocilo").html(
					"<br><span class='obvestilo label label-danger fade-in'>Napaka '" +
					JSON.parse(err.responseText).userMessage + "'!");
			}
		});
	}
}

//generiranj grafov, animacij in wikipidije glede na podatke uporabnika
function preberiMeritveVitalnihZnakov() {
    prikaziWiki();
    
	sessionId = getSessionId();

	var ehrId = $("#meritveVitalnihZnakovEHRid").val();

	//vrednosti resetiramo, saj zelimo da ima nov uporabnik drugacne podatke
	document.getElementById("wikiTextBloodPressure").innerHTML = "";
	document.getElementById("wikiTextDeath").innerHTML = "";
	document.getElementById("wikiTextItm").innerHTML = "";
	document.getElementById("wikiTextTveganja").innerHTML = "";
	document.getElementById("wikiTextZdravje").innerHTML = "";
	$("#wikiTextTveganja2").html(""); //lazje kot document.getelement blabla
	$("#wikiDetails").hide();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("<br><span class='obvestilo " +
			"label label-warning fade-in'>Prosim vnesite zahtevan podatek!");
	} else {
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
			headers: {
				"Ehr-Session": sessionId
			},
			success: function(data) {
				var party = data.party;

				
				
				$.ajax({
					url: baseUrl + "/view/" + ehrId + "/" + "pulse",
					type: 'GET',
					headers: {
						"Ehr-Session": sessionId
					},
					success: function(res) {
						var hintUtrip = "<br>";

						if (res[res.length - 1].pulse > 100)
							hintUtrip = " ";
						else if (res[res.length - 1].pulse < 50)
							hintUtrip = " ";
						else
							hintUtrip = " ";
                        
						document.getElementById('utrip').innerHTML = "Srčni utrip uporabnika je " + res[res.length - 1].pulse + " /min." + hintUtrip;
						document.getElementById('utripNumber').innerHTML = res[res.length - 1].pulse;

                         //funkcija caka da koncamo z klici in potem prikaze grafiko
						preveri()

					},
				});

				
				
				$.ajax({
					url: baseUrl + "/view/" + ehrId + "/" + "weight",
					type: 'GET',
					headers: {
						"Ehr-Session": sessionId
					},
					success: function(res) {


						document.getElementById('teza').innerHTML = "<b>(Aplikacija ni nadomestilo strokovnega nasveta vašega zdravnika!)</b>"
						document.getElementById('tezaNumber').innerHTML = res[res.length - 1].weight;
						preveri()
					},
				});

				
				
				$.ajax({
					url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					type: 'GET',
					headers: {
						"Ehr-Session": sessionId
					},
					success: function(res) {
					


						document.getElementById('zgornjiTlak').innerHTML = "Kakšen je pomen vašega srčnega utripa? Načeloma velja, da je normalen utrip za odraslega med 60 in 100 udarci srca na minuto, pri otrocih od 6. do 15. leta znaša v mirovanju med 70 in 100 udarci na minuto, nekateri športniki z zelo natreniranim srcem pa imajo lahko pulz v mirovanju tudi samo 40. Srčni utrip narašča z gibanjem, delovanje srca pa lahko pospešijo tudi čustva, nervoza, stres, pretiravanje ali dehidracija.";
						document.getElementById("zgornjiTlakNumber").innerHTML = res[res.length - 1].systolic;
						preveri()

					},
				});

				
				
								
				
				$.ajax({
					url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
					type: 'GET',
					headers: {
						"Ehr-Session": sessionId
					},
					success: function(res) {
					
							
							var a = document.getElementById("tezaNumber").innerHTML;
								var b = document.getElementById("visinaNumber").innerHTML;
								
								var c = (a / ((b / 100) * (b / 100)));
						document.getElementById('spodnjiTlak').innerHTML = "Vaš indeks telesne mase znaša " +c;
						document.getElementById("spodnjiTlakNumber").innerHTML = res[res.length - 1].diastolic;
						preveri()
					},
				});

			
			
				$.ajax({
					url: baseUrl + "/view/" + ehrId + "/" + "height",
					type: 'GET',
					headers: {
						"Ehr-Session": sessionId
					},
					success: function(res) {

						document.getElementById('visina').innerHTML = "Kaj pravzaprav pomeni indeks telesne mase? 21,4 ali manj: glede na svojo višino podhranjeni (presuhi). Torej s hrano ne dobite dovolj hranil v telo. Med 21,4 in 25,6: zdrava, normalna telesna teža glede na vašo višino. Med 25,6 in 29,9: spadate v skupino čezmerno prehranjenih. Priporočljivo je, da svoj indeks telesne mase zmanjšati pod 25. ITM nad 30: znamenje debelosti. Pri hujšanju boste potrebovali veliko volje in vzpodbude ter morda strokovno pomoč.";
						document.getElementById("visinaNumber").innerHTML = res[res.length - 1].height;
						preveri()
					},
				});

				$("#overall, #details, #friendlyTip").show();
			},

		});
	}

}


$(document).ready(function() {

	/**
	 * Napolni testni EHR ID pri pregledu meritev vitalnih znakov obstoječega
	 * bolnika, ko uporabnik izbere vrednost iz padajočega menuja
	 */
	$('#preberiEhrIdZaVitalneZnake').change(function() {
		$("#preberiMeritveVitalnihZnakovSporocilo").html("");
		$("#meritveVitalnihZnakovEHRid").val($(this).val());
	});

});

//wikipidija API
function getWiki(url, divToPost, baseText) {
	var title = url.split("/");
	title = title[title.length - 1];

	//Get Leading paragraphs (section 0)
	$.getJSON("https://sl.wikipedia.org/w/api.php?action=parse&page=" + title + "&prop=text&section=0&format=json&callback=?", function(data) {
		for (text in data.parse.text) {
			var text = data.parse.text[text].split("<p>");
			var pText = "";

			for (p in text) {
				//Remove html comment
				text[p] = text[p].split("<!--");
				if (text[p].length > 1) {
					text[p][0] = text[p][0].split(/\r\n|\r|\n/);
					text[p][0] = text[p][0][0];
					text[p][0] += "</p> ";
				}
				text[p] = text[p][0];

				//Construct a string from paragraphs
				if (text[p].indexOf("</p>") == text[p].length - 5) {
					var htmlStrip = text[p].replace(/<(?:.|\n)*?>/gm, '') //Remove HTML
					var splitNewline = htmlStrip.split(/\r\n|\r|\n/); //Split on newlines
					for (newline in splitNewline) {
						if (splitNewline[newline].substring(0, 11) != "Cite error:") {
							pText += splitNewline[newline];
							pText += "\n";
						}
					}
				}
			}
			pText = pText.substring(0, pText.length - 2); //Remove extra newline
			pText = pText.replace(/\[\d+\]/g, ""); //Remove reference tags (e.x. [1], [4], etc)
			document.getElementById('' + divToPost + '').innerHTML = baseText + pText + "<br><br>"
		}
	});
}
